import { Component } from '@angular/core';
import { NavController, AlertController, ActionSheetController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  documento:any = {
    label: 'Foto 1', 
    valor: '',
    width: 0, 
    height: 0, 
    atributo: 'cnhF', 
    iconname: 'ios-add'
  }


  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public actionCtrl: ActionSheetController,
    ) {

  }

  presentActionSheet() {
    const actionSheet =  this.actionCtrl.create(
      {
        title: 'Escolha uma das opções para carregar o documento',
        buttons: [
          //  Não implementado pela impossibilidade de teste em dispositivo real
          //
          //   {
          //   text: 'Câmera',
          //   icon: 'camera',
          //   handler: () => {
          //     console.log('Passou pela camera')
          //   }
          // },
          {
            text: 'Galeria',
            icon: 'images',
            handler: () => {
              this.temPermissao().then(
                res => {
                  if(res) {
                    this.escolherFoto().then(  
                      res => {
                        console.log('img.data', res.data)
                        this.documento.valor = res.data
                        this.documento.width = res.width
                        this.documento.height = res.height
                        this.documento.iconname = 'ios-checkmark'
                      },
                      err => {
                        let alert = this.alertCtrl.create({
                          title: 'Erro',
                          message: 'Ocorreu um erro ao recuperar a imagem.',
                          buttons: [{text:'Ok'}]
                        })

                        //  alert.present()
                      }
                    )
                  } else {
                    this.pedirPermissao().then(
                      res => {
                        console.log('RESULTADO DO PEDIDO ')
                      },
                      err => {
                        let alert = this.alertCtrl.create({
                          title: 'Autorização',
                          message: 'Este app precisa que você autorize o acesso à biblioteca de fotos nas configurações do dispositivo.',
                          buttons: [{text:'Ok'}]
                        })

                        alert.present()
                      }
                    )
                  }
                }
              )
            }
          }, {
            text: 'Cancelar',
            icon: 'close',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }]
        });
        actionSheet.present()

      }


      
  pedirPermissao() {
    return new Promise((resolve,reject) => {
      window['imagePicker'].requestReadPermission(
        res => { resolve(res) },
        err => { reject(err) }
      )
    })
  }


  temPermissao():Promise<boolean> {
    return new Promise((resolve) => {
      window['imagePicker'].hasReadPermission(
        res => { resolve(res) }
      )
    })
  }

  escolherFoto():Promise<any> {
    let options = {
      maximumImagesCount: 1,
      outputType: 1,
      quality: 98,
      width: 800,
      title:'Escolha uma imagem'
    }

    return new Promise((resolve,reject) => {
      window['imagePicker'].getPictures(
        res => {
          if(res.length > 0){

            this.getImageDimensions("data:image/jpg;base64," + res[0]).then(
              result => {
                let img = {
                  //data:"data:image/jpg;base64," + res[0],
                  data: res[0],
                  width: result['w'],
                  height: result['h']
                }
                console.log('WxH: ' + result['w'] + 'x' + result['h'] + 'imagebase64' + res[0]) 
                resolve(img)
              },
              err=>{console.log('ERRO Dimensao ' + err)}
            )
          }
          else { reject('Sem imagem selecionada') }
        },
        err => { reject('Erro durante a escolha da imagem') }, options)
      })
    }

    getImageDimensions(file) {
      return new Promise (function (resolved, rejected) {
        var i = new Image()
        i.onload = function(){
          resolved({w: i.width, h: i.height})
        };
        i.src = file
      })
    }



}
